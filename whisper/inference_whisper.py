import numpy as np
import os
import json
import whisper

#pip install git+https://github.com/openai/whisper.git 

def transcribe(start_tokens, file, encoder, decoder, tokenizer, skip_special_tokens=True):
    def run(seed:np.ndarray, hidden_states)->int:
        decoder_output = decoder.run(None, {'input_ids' : np.expand_dims(seed, axis=0), 'encoder_hidden_states': hidden_states})[0]
        cleaned = np.argmax(decoder_output, axis=-1)
        last_token = cleaned[0,-1]
        return last_token
    
    audio = whisper.load_audio(file)
    audio = whisper.pad_or_trim(audio)
    mel = whisper.log_mel_spectrogram(audio)
    mel = np.expand_dims(mel,0)

    hidden_states = encoder.run(None, {'input_features': mel})[0]

    tokens = start_tokens
    while(True):
        last_token = run(tokens, hidden_states)
        tokens.append(last_token)
        if tokens[-1] == 50257:
            return tokenizer.batch_decode(np.expand_dims(tokens, axis=0), skip_special_tokens=skip_special_tokens)[0]
     


import onnxruntime as ort
from transformers import (
    AutoTokenizer
)

model_id = "ifrz/whisper-gl-tiny"
encoder_model = './onnx_models/ifrz/whisper-gl-tiny/encoder_model.onnx'
decoder_model = './onnx_models/ifrz/whisper-gl-tiny/decoder_model.onnx'
encoder_ort_sess = ort.InferenceSession(encoder_model)
decoder_ort_sess = ort.InferenceSession(decoder_model)

options = ort.SessionOptions()
options.graph_optimization_level = ort.GraphOptimizationLevel.ORT_ENABLE_ALL
options.intra_op_num_threads = 4
#options.inter_op_num_threads = 2 # use with parallel
#options.enable_profiling = True
#options.execution_mode = rt.ExecutionMode.ORT_PARALLEL

encoder_model_qint8 = './onnx/encoder_model_quantized.onnx' 
decoder_model_qint8 = './onnx/decoder_model_quantized.onnx' 
encoder_ort_sess_qint8 = ort.InferenceSession(encoder_model_qint8, options)
decoder_ort_sess_qint8 = ort.InferenceSession(decoder_model_qint8, options)

tokenizer = AutoTokenizer.from_pretrained(model_id)

start_tokens = [50258, 50319, 50359, 50363] #<|startoftranscript|><|gl|><|transcribe|><|notimestamps|> token codes here https://github.com/openai/whisper/blob/main/whisper/tokenizer.py

import time

startTime = time.time()
text = transcribe(start_tokens, '../test.wav', encoder_ort_sess, decoder_ort_sess, tokenizer, skip_special_tokens=True)
executionTime = (time.time() - startTime)
print(text)
#print('Time model (s): ' + str(executionTime))


startTime = time.time()
text = transcribe(start_tokens, '../test.wav', encoder_ort_sess_qint8, decoder_ort_sess_qint8, tokenizer, skip_special_tokens=True)
executionTime = (time.time() - startTime)
print(text)
#print('Time model qint8 (s): ' + str(executionTime))
